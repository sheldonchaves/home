import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-shortcuts',
  templateUrl: './shortcuts.component.html',
  styleUrls: ['./shortcuts.component.scss']
})
export class ShortcutsComponent implements OnInit {

  public isCopied: any;

  contextMenu;

  constructor(public service: AppService) { }

  ngOnInit() {

    this.contextMenu = [
      {
        label: `new group`,
        icon: 'pi pi-folder', command: (event) => {
          this.service.editItem = {
            type: 'group',
            title: '',
            color: ''
          };
        }
      }
    ];

  }

  getdata() {
    return this.service.data;;
  }

  copy(value: string) {
    let re = /``/gi;
    return value.replace(re, '"');
  }

}
