import { Component } from '@angular/core';
import { AppService } from './service/app.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public service: AppService) { }


  save() {

    if (this.service.editItem.type == 'item') {

      if (this.service.editItem.index != undefined) {

        this.service.data[this.service.editItem.group].items[this.service.editItem.index] = {
          name: this.service.editItem.name,
          code: this.service.editItem.code.replace('"', "``")
        };

      } else {
        this.service.data[this.service.editItem.group].items.push({
          name: this.service.editItem.name,
          code: this.service.editItem.code.replace('"', "``")
        });
      }

    } else {

      if (this.service.editItem.group) {
        this.service.data[this.service.editItem.group] = {
          title: this.service.editItem.title,
          icon: this.service.editItem.icon,
          color: this.service.editItem.color,
          items: this.service.data[this.service.editItem.group].items
        };


      } else {

        this.service.data.push({
          title: this.service.editItem.title,
          icon: this.service.editItem.icon,
          color: this.service.editItem.color,
          items: []
        });
      }


    }

    this.service.editItem = null;

    this.service.saveShortcuts();

  }

  cancel() {
    this.service.editItem = null;
  }

}

